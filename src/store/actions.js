import { HTTP } from '../http'

export const getItems = (state) => {
    HTTP.post('api/index.php')
        .then(response => {
            state.commit('setItems', response.data)
        })
        .catch(function() {
            //console.log(error)
        })
        .then(function() {
            // state.commit('setLoader', false)
        })
}

export const getLanding = (state) => {
    HTTP.post('api/landing.php')
        .then(response => {
            state.commit('setLanding', response.data)
        })
        .catch(function() {
            //console.log(error)
        })
        .then(function() {
            // state.commit('setLoader', false)
        })
}
export const getModels = (state) => {
    HTTP.post('api/models.php')
        .then(response => {
            state.commit('setModels', response.data)
        })
        .catch(function() {
            //console.log(error)
        })
        .then(function() {
            // state.commit('setLoader', false)
        })
}
