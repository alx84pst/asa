export default {
    // Список акций
    items: [],
    // Настройки лендинга
    landing: [],
    // Список моделей
    models: [],
    // Пагинация
    showItemsCount: 8,
    increment: 4,
    // Поля формы
    f_name: '',
    f_tel: '',
    f_email: '',
    f_model: '',
    f_date: '',
    // Согласие на обработку персональных данных
    user_pers_ready: false,
    // Согласие на получение информации об акциях
    user_advt_ready: false
}
