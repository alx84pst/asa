export const getItems = state => state.items
export const getShowItemsCount = state => state.showItemsCount
export const getIncrement = state => state.increment
export const getItemsCount = state => state.items.length
export const getShowItems = state => num => {
    return state.items.filter(function(item, i) {
        return ('NAME' in item) ? i < num : ''
    })
}
export const getLanding = state => state.landing
export const getLandingSoglasie = state => {
    return [ state.landing.PROPERTY_S_ADVERT_VALUE, state.landing.PROPERTY_S_PERSON_VALUE ]
}
export const getModels = state => state.models
export const getFormName = state => state.f_name
export const getFormTel = state => state.f_tel
export const getFormEmail = state => state.f_email
export const getFormModel = state => state.f_model
export const getFormDate = state => state.f_date
export const getUserPersReady = state => state.user_pers_ready
export const getUserAdvtReady = state => state.user_advt_ready
