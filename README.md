# service

## Установка проекта

```npm install```

### Compiles and hot-reloads for development

```npm run serve```

### Compiles and minifies for production

```npm run build```

### Lints and fixes files

```npm run lint```

### Vue Components

#### commonFields

Основной набор полей

#### landDialog

Всплывающие диалоги

#### mainForm

Контейнер элементов формы

#### serviceList

Список

#### serviceListMore

Блок "Показать еще" внизу списка

#### soglasieFields

Переключатели согласий

#### yaMap

Карта

### Размещение проекта на боевом сервере

#### Структура проекта

/service/ - путь к проекту
api - скрипты для запросов к БД
css - стили
fonts - шрифты
img - изображения
js - скрипты приложения
png - изображения в формате png
.section.php - файл с настройками для получения данных из инфоблоков
index.php - файл запуска проекта

#### Обновление приложения проекта

После сборки проекта необходимо перенести содержимое ```/dist/css/*.*``` и ```/dist/js/*.*``` в соответствующие разделы проекта. Перед переносом разделы необходимо очистить. Необходимо оставить только файл style.css в разделе css.
В файле index.php необходимо подключить новые файлы стилей и скриптов:

```php
<?
$APPLICATION->AddHeadScript('./js/app.537a27a7.js');
$APPLICATION->AddHeadScript('./js/chunk-vendors.13afbc45.js');
$APPLICATION->SetAdditionalCSS('/service/css/app.ab1cd708.css');
$APPLICATION->SetAdditionalCSS('/service/css/chunk-vendors.faa56a19.css');
$APPLICATION->SetAdditionalCSS('/service/css/style.css');
?>
```
